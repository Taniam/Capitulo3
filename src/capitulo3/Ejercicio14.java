/*
 * Intrucciones del ejercicio: (La clase Fecha) Cree una clase llamada Fecha, que incluya tres variables de
instancia: un mes (tipo int), un día (tipo int) y un año (tipo int). Su clase debe tener un
constructor que inicialice las tres variables de instancia, y debe asumir que los
valores que se proporcionan son correctos. Proporcione un método establecer y un
método obtener para cada variable de instancia. Proporcione un método mostrar
Fecha, que muestre el mes, día y año, separados por barras diagonales (/). Escriba
una aplicación de prueba llamada PruebaFecha, que demuestre las capacidades de la
claseFecha.
 */
package Capitulo3;
 import java.util.Scanner;
 
public class Ejercicio14 {
 
  
int mes;
int dia;
int año;
 Ejercicio14(){
	this.mes=mes;
   this.dia = dia;
   this.año= dia;
}
 
 
 
	public int getMes() {
    	return mes;
	}
 
	public void setMes(int mes) {
    	this.mes = mes;
	}
 
	public int getDia() {
    	return dia;
	}
 
	public void setDia(int dia) {
    	this.dia = dia;
	}
 
	public int getAño() {
    	return año;
	}
 
	public void setAño(int año) {
    	this.año = año;
	}
 
 public Ejercicio14(int mes, int dia, int anio) {
    
    	if ( mes < 1 && mes > 12 ) {
        	this.mes = 1;
    	} else {
        	this.mes = mes;
    	}
    	if (dia < 1 && dia > 31 ) {
        	this.dia = 1;
    	} else {
        	this.dia = dia;
    	}
	}
   @Override public String toString(){
    	String fecha = getMes() + " / " + getDia() + " / "
        	+ getAño();
    	return fecha;
	}
 
	public static void main(String[] args) {
   	Scanner s = new Scanner(System.in);
   	int x,y,z;
       System.out.println("¿Que dia es?");
  	x = s.nextInt();
      System.out.println("¿Que mes es?");
   	y = s.nextInt();
      System.out.println("¿Que año es?");
   	z = s.nextInt();
 Ejercicio14 fecha = new Ejercicio14(x , y, z);
        System.out.println("La fecha es: " + fecha);
	}
	
}
