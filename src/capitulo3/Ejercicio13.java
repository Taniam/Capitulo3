/*
 * Intrucciones del ejercicio: (La clase Empleado) o Cree una clase llamada Empleado, que incluya tres
variables de instancia: un primer nombre (tipo String), un apellido paterno (tipo
String) y un salario mensual (double). Su clase debe tener un constructor que
inicialice las tres variables de instancia. Proporcione un método establecer y un
método r obtener para cada r variable de instancia. Si el salario mensual no es
positivo, no establezca su valor. Escriba una aplicación de prueba llamada
PruebaEmpleado, que demuestre las capacidades de la clase Empleado. Cree dos
objetos Empleado y muestre el salario anual de cada objeto. Después, proporcione a
cada l Empleado un aumento del 10% y muestre el salario anual de cada Empleado
otra vez.
 */
package capitulo3;

/**
 *
 * @author Hector
 */
public class Ejercicio13 {

    String PrimerNombre;
    String ApellidoP;
    double salarioM;
    
    public Ejercicio13(String PrimerNombre, String ApellidoP, double salarioM){
    this.PrimerNombre=PrimerNombre;
    this.ApellidoP=ApellidoP;
    
    if(salarioM > 0.0)
    this.salarioM=salarioM;
    }
    
    // método que establece el nombre
    public void establecerPrimerNombre(String PrimerNombre){
    this.PrimerNombre = PrimerNombre;
    }

    // método que devuelve el nombre
    public String obtenerNombre(){
    return PrimerNombre;
    }

    // método que establece el apellido Paterno
    public void establecerApellidoP(String ApellidoP){
    this.ApellidoP = ApellidoP;
    }

    // método que devuelve el apellido Paterno
    public String obtenerApellidoP(){
    return ApellidoP;
    }

    // método que establece el salario
    public void establecerSalarioM(double SalarioM){
    this.salarioM = SalarioM;
    }

    // método que devuelve el salario
    public double obtenerSalario(){
    return salarioM;
    }

    public void ObtenerSalarioAnual(){
    if (obtenerSalario() < 0.0)
    salarioM = 0.0; 
    System.out.printf("El salario anual de %s es: %.2f%n", obtenerNombre(), obtenerSalario()*12);
    }

    public void AumentoSalario(){
    if(obtenerSalario() < 0.0)
    salarioM=0.0;

    double salario10=((10*obtenerSalario())/100);
    double salario=obtenerSalario()+salario10;
    System.out.printf("El salario anual con el aumento de %s es: %.2f%n", obtenerNombre(), salario*12);
    }


//public class PruebaEmpleado {

    public static void main(String [] args){
    Ejercicio13 Empleado1=new Ejercicio13("John", "Smith", 1528.80);
    Ejercicio13 Empleado2=new Ejercicio13("Maggy", "Mendoza", 1577.70);

    System.out.printf("El Empleado1 se llama %s %s y su saldo mensual es de %.2f%n", Empleado1.obtenerNombre(), Empleado1.obtenerApellidoP(),
    Empleado1.obtenerSalario());
    System.out.printf("El Empleado2 se llama %s %s y su saldo mensual es de %s%n%n", Empleado2.obtenerNombre(), Empleado2.obtenerApellidoP(),
    Empleado2.obtenerSalario());
    Empleado1.ObtenerSalarioAnual();
    Empleado2.ObtenerSalarioAnual();

    System.out.printf("%nSe le da un aumneto del 10 porciento al señor %s y a la señorita %s%n", Empleado1.obtenerApellidoP(), Empleado2.obtenerApellidoP());
    System.out.printf("%nCalculado el salario anual después del aumento...%n");
    Empleado1.AumentoSalario();
    Empleado2.AumentoSalario();
    }
}
