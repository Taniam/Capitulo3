/*
 *Indicaciones del ejercicio: (La clase Factura) a Cree una clase llamada Factura que una ferretería podría
utilizar para representar una factura para un artículo vendido en la tienda. Una
Factura debe incluir cuatro piezas de información como variables de instancia: un
número de pieza (tipo String), la descripción de la pieza (tipo String), la cantidad de
artículos de ese tipo que se van a comprar (tipoint) y el precio por artículo (double).
Su clase debe tener un constructor que inicialice las cuatro variables de instancia.
Proporcione un método establecer y un método r obtener para cada variable de r
instancia. Además, proporcione un método llamado obtenerMontoFactura, que
calcule el monto de la factura (es decir, que multiplique la cantidad de artículos por el
precio de cada uno) y después devuelva ese monto como un valor double. Si la
cantidad no es positiva, debe establecerse en 0. Si el precio por artículo no es
positivo, debe establecerse en 0.0. Escriba una aplicación de prueba llamada
PruebaFactura, que demuestre las capacidades de la clase Factura.
 */
package capitulo3;
import java.util.Scanner;
/**
 *
 * @author Hector
 */
public class Ejercicio12 {
   
    private String NoDePieza; // Número de la pieza
    private String Descripcion; // Descripción de la pieza

    int Cantidad; // Cantidad del artículo que va a comprar
    double precio; // Valor del artículo

    public Ejercicio12(String NoDePieza, String Descripcion, int Cantidad, double Precio){

        this.NoDePieza= NoDePieza;
        this.Descripcion=Descripcion;

    if(Cantidad > 0)
    this.Cantidad=Cantidad;

    if(Precio > 0.0)
    this.precio=Precio;

    }

    // método que establece el nombre
    public void establecerNoDePirza(String NoDePieza){
    this.NoDePieza = NoDePieza;
    }

    // método que devuelve el nombre
    public String obtenerNoDePieza(){
    return NoDePieza; //devuelve el valor de name a quien lo invocó
    }
    
    // método que establece el nombre
    public void establecerDescripcion(String Descripcion){
    this.Descripcion = Descripcion;
    }

    // método que devuelve el nombre
    public String obtenerDescripcion(){
    return Descripcion; //devuelve el valor de name a quien lo invocó
    }

    // método que establece el nombre
    public void establecerCantidad(int Cantidad){
    this.Cantidad = Cantidad;
    }

    // método que devuelve el precio
    public int obtenerCantidad(){
    return Cantidad; //devuelve el valor de name a quien lo invocó
    }

    // método que establece el precio
    public void establecerPrecio(double Precio){
    this.precio = Precio;
    }
    
    // método que devuelve el precio
    public double obtenerPrecio(){
    return precio; //devuelve el valor de name a quien lo invocó
    } 

    // método que muestra la factura
    public void ObtenerMontoFactura(int Cantidad){

    if ( Cantidad < 0)
    Cantidad=0;

    if (precio < 0.0)
    precio=0.0;

    System.out.println("Factura de la Ferretería");
    System.out.printf("Número de Pieza: %s%n",obtenerNoDePieza());
    System.out.printf("Decripción de la pieza: %s%n", obtenerDescripcion());
    System.out.printf("Cantidad: %s%n", Cantidad);
    System.out.printf("Precio unitario: %s%n", obtenerPrecio());
    System.out.printf("Monto a pagar es: %s%n", Cantidad*obtenerPrecio());
    }

//public class PruebaFactura {

    public static void main(String[] args){
    Ejercicio12 Factura=new Ejercicio12("01", "Ninguna", -12, 40.0);

    // crea un objeto Scanner para obtener la entrada de la ventana de comandos
    Scanner entrada = new Scanner(System.in);
    System.out.println("\nEscriba la cantidad de piezas que compró: ");
    int Cantidad=entrada.nextInt();
    System.out.println("\nImprimiendo Facutra...\n");
    Factura.ObtenerMontoFactura(Cantidad);
    
    
    //Muestra el estado actual de la factura
    System.out.println("Factura de la Ferretería\n");
    System.out.printf("Número de Pieza: %s%n",Factura.obtenerNoDePieza());
    System.out.printf("Decripción de la pieza: %s%n", Factura.obtenerDescripcion());
    System.out.printf("Cantidad: %s%n", Factura.obtenerCantidad());
    System.out.printf("Precio unitario: %s%n", Factura.obtenerPrecio());
    System.out.printf("Monto a pagar es: %s%n",
    Factura.obtenerCantidad()*Factura.obtenerPrecio());

    }
}
    

