/*
 * Indicaciones del ejercicio: (Eliminar código duplicado en el método main)  En la clase PruebaCuenta de la figura 3.9, el método main contiene seis instrucciones (líneas 13-14, 15-16, 28-29, 30-31, 40-41 y 42-43), cada una de las cuales muestra en pantalla el nombre y saldo de un objeto Cuenta. Estudie estas instrucciones y notará que difieren sólo en el objeto Cuenta que se está manipulando: cuenta1 o cuenta2. En este ejercicio definirá un nuevo método mostrarCuenta que contiene una copia de esa instrucción de salida. El parámetro del método será un objeto Cuenta y el método imprimirá en pantalla el nombre y saldo de ese objeto. Después usted sustituirá las seis instrucciones duplicadas en main con llamadas a mostrarCuenta, pasando como argumento el objeto Cuentaespecífico a mostrar en pantalla. Modifique la clase PruebaCuenta de la figura 3.9 para declarar el siguiente método mostrarCuentadespués de la llave derecha de cierre de main y antes de la llave derecha de cierre de la clase PruebaCuenta:
public static void mostrarCuenta (Cuenta cuentaAMostrar) { 
// coloque aquí la instrucción que muestra en pantalla   
// el nombre y el saldo de cuentaAMostrar
 }

Sustituya el comentario en el cuerpo del método con una instrucción que muestre el nombre y el saldo de cuentaAMostrar. Recuerde que main es un métodostatic, por lo que puede llamarse sin tener que crear primero un objeto de la clase en la que se declara main. También declaramos el método mostrarCuenta como un método static. Cuando main necesita llamar a otro método en la misma clase sin tener que crear primero un objeto de esa clase, el otro método también debe declararse como static. Una vez que complete la declaración de mostrarCuenta, modifique main para reemplazar las instrucciones que muestran el nombre y saldo de cada Cuenta con llamadas a mostrarCuenta; cada una debe recibir como argumento el objeto cuenta1 o cuenta2, según sea apropiado. Luego, pruebe la clase PruebaCuenta actualizada para asegurarse de que produzca la misma salida que se muestra en la figura 3.9.

 */
package Capitulo3;
 import java.util.Scanner;
 

public class Ejercicio15 {
	private String nombre;
	private double saldo;
   // Constructor de Cuenta que recibe dos parámetros
        	public Ejercicio15(String nombre, double saldo){
        	this.nombre = nombre; // asigna nombre a la variable de instancia nombre
        	// valida que el saldo sea mayor que 0.0; de lo contrario,
        	// la variable de instancia saldo mantiene su valor inicial predeterminado de 0.0
        	if (saldo > 0.0) // si el saldo es válido
        	this.saldo = saldo; // lo asigna a la variable de instancia saldo
        	}
        	// método que deposita (suma) sólo una cantidad válida al saldo
        	public void depositar(double montoDeposito){
        	if (montoDeposito > 0.0) // si el montoDeposito es válido
        	saldo = saldo + montoDeposito; // lo suma al saldo
        	}
        	
        	//método que retira (resta) sólo una cantidad validad al saldo
        	public void retirar(double montoDeposito){
	    	if (montoDeposito >0.0 && montoDeposito < saldo)
    		saldo=saldo-montoDeposito;
	    	if (montoDeposito < 0.0 || montoDeposito > saldo)
    		System.out.println("El monto a retirar excede el saldo de "
            	    	+ "la cuenta");
        	}
        	// método que devuelve el saldo de la cuenta
        	public double obtenerSaldo(){
	    	return saldo;
        	}
        	// método que establece el nombre
        	public void establecerNombre(String nombre){
	    	this.nombre = nombre;
        	}
        	// método que devuelve el nombre
        	public String obtenerNombre(){
	    	return nombre; //devuelve el valor de name a quien lo invocó
        	}
 
	/**
 	* @param args the command line arguments
 	*/
	public static void main(String[] args) {
    	Ejercicio15 cuenta1 = new Ejercicio15("Jane Green", 50.00);
        Ejercicio15 cuenta2 = new Ejercicio15("John Blue", -7.53);
        Scanner entrada = new Scanner(System.in);
        // muestra el saldo inicial de cada objeto
        /*
        System.out.printf("Saldo de %s: $%.2f%n",
     	cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
         System.out.printf("Saldo de %s: $%.2f%n%n",
         cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
            */
        mostrarCuenta(cuenta1);
        mostrarCuenta(cuenta2);
 
        System.out.print("Escriba el monto a depositar para cuenta1: "); // indicador (promt)
        double montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
        System.out.printf("%nsumando %.2f al saldo de cuenta1%n%n",
        montoDeposito);
        cuenta1.depositar(montoDeposito); // suma al saldo de cuenta1
 
        // muestra los saldos
        /*System.out.printf("Saldo de %s: $%.2f%n",
        cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n",
        cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());*/
        mostrarCuenta(cuenta1);
        mostrarCuenta(cuenta2);
        System.out.print("Escriba el monto a depositar para cuenta2: "); // indicador (promt)
        montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
        System.out.printf("%nsumando %.2f al saldo de cuenta2%n%n",
        montoDeposito);
        cuenta2.depositar(montoDeposito); // suma al saldo de cuenta2

        // muestra los saldos
       /*
        System.out.printf("Saldo de %s: $%.2f%n",cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n",cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
       */
        mostrarCuenta(cuenta1);
        mostrarCuenta(cuenta2);

        System.out.print("Escriba el monto a retirar para cuenta1: ");
        double montoRetiro = entrada.nextDouble();
        System.out.printf("%nRetirando %.2f al saldo de cuenta1%n%n",
        montoRetiro);
        cuenta1.retirar(montoRetiro);

        // muestra los saldos
        /*
        System.out.printf("Saldo de %s: $%.2f%n",
        cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n",
        cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
       */
         mostrarCuenta(cuenta1);
        mostrarCuenta(cuenta2);
        System.out.print("Escriba el monto a retirar para cuenta2: ");
        montoRetiro = entrada.nextDouble();
        System.out.printf("%nRetirando %.2f al saldo de cuenta2%n%n",
        montoRetiro);
        cuenta2.retirar(montoRetiro); // suma al saldo de cuenta2

        // muestra los saldos
        /*
       System.out.printf("Saldo de %s: $%.2f%n",cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n",cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
         */
         mostrarCuenta(cuenta1);
        mostrarCuenta(cuenta2);
        
        }
               public static void mostrarCuenta(Ejercicio15 r){
               System.out.printf("Saldo de %s: $%.2f%n" , r.obtenerNombre(), r.obtenerSaldo());

            }
       }
 
