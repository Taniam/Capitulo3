/*
 * instrucciones del ejrcicio: (Clase Cuenta modificada) Modifique la clase Cuentab para
proporcionar un método llamado retirar, que retire dinero de un objeto Cuenta.
Asegúrese de que el monto a retirar no exceda el saldo de Cuenta. Si lo hace, el saldo
debe permanecer sin cambio y el método debe imprimir un mensaje que indique “El
monto a retirar excede el saldo de la cuenta”. Modifique la clasePruebaCuenta para probar el método retirar.
 */
package capitulo3;
import java.util.Scanner;
/**
 *
 * @author Hector
 */
public class Ejercicio11 {
    private String nombre; // variable de instancia
    private double saldo; // variable de instancia

    // Constructor de Cuenta que recibe dos parámetros
    public Ejercicio11(String nombre, double saldo){
    this.nombre = nombre; // asigna nombre a la variable de instancia nombre
    // valida que el saldo sea mayor que 0.0; de lo contrario,
    // la variable de instancia saldo mantiene su valor inicial predeterminado de 0.0

    if (saldo > 0.0) // si el saldo es válido
    this.saldo = saldo; // lo asigna a la variable de instancia saldo
    }

    // método que deposita (suma) sólo una cantidad válida al saldo
    public void depositar(double montoDeposito){
    if (montoDeposito > 0.0) // si el montoDeposito es válido
    saldo = saldo + montoDeposito; // lo suma al saldo
    }

    //método que retira (resta) sólo una cantidad validad al saldo
    public void retirar(double montoDeposito){
    if (montoDeposito > 0.0 && montoDeposito < saldo)
    saldo=saldo-montoDeposito;

    if (montoDeposito < 0.0 && montoDeposito > saldo)
    System.out.println("El monto a retirar excede el saldo de la cuenta");
    }

    // método que devuelve el saldo de la cuenta
    public double obtenerSaldo(){
    return saldo;
    }

    // método que establece el nombre
    public void establecerNombre(String nombre){
    this.nombre = nombre;
    }

    // método que devuelve el nombre
    public String obtenerNombre(){
    return nombre; //devuelve el valor de name a quien lo invocó
    }

//public class PruebaCuenta {
    
    public static void main(String[] args){

    Scanner s=new Scanner (System.in);
    // crea un objeto Scanner para obtener la entrada de la ventana de comandos
    Scanner entrada = new Scanner(System.in);
    int op=0;
    Ejercicio11 cuenta1 = new Ejercicio11("Jane Green", 500.00);
    Ejercicio11 cuenta2 = new Ejercicio11("John Blue", -7.53);
    
    System.out.println("¿Qué quiere hacer?\n 1)Depositar\n 2)Retirar");
    while(op !=2){
    op=s.nextInt();
    
    switch(op){
    
    case 1:
        // muestra el saldo inicial de cada objeto
        System.out.printf("Saldo de %s: $%.2f%n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n",cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
        System.out.print("Escriba el monto a depositar para cuenta1: "); // indicador (promt)
        double montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
        System.out.printf("%nsumando %.2f al saldo de cuenta1%n%n", montoDeposito);
        cuenta1.depositar(montoDeposito); // suma al saldo de cuenta1
        // muestra los saldos
        System.out.printf("Saldo de %s: $%.2f%n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n", cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
        System.out.print("Escriba el monto a depositar para cuenta2: "); // indicador (promt)
        montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
        System.out.printf("%nsumando %.2f al saldo de cuenta2%n%n", montoDeposito);
        cuenta2.depositar(montoDeposito); // suma al saldo de cuenta2
        
        break;

    case 2: // muestra los saldos

        System.out.printf("Saldo de %s: $%.2f%n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n",cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
        System.out.print("Escriba el monto a retirar para cuenta1: "); // indicador (promt)
        montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
        System.out.printf("%nrestando %.2f al saldo de cuenta1%n%n", montoDeposito);
        cuenta1.retirar(montoDeposito); // suma al saldo de cuenta1
        // muestra los saldos
        System.out.printf("Saldo de %s: $%.2f%n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n", cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
        System.out.print("Escriba el monto a retirar para cuenta2: "); // indicador (promt)
        montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
        System.out.printf("%nrestando %.2f al saldo de cuenta2%n%n", montoDeposito);
        cuenta2.retirar(montoDeposito); // suma al saldo de cuenta2
        // muestra los saldos
        System.out.printf("Saldo de %s: $%.2f%n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("Saldo de %s: $%.2f%n%n", cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
        break;


                }
            }
        }
    }
//}